extern crate libc;
extern crate simple_signal;

use std::io::{self, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::sync::{
    atomic::{AtomicUsize, Ordering, ATOMIC_USIZE_INIT},
    mpsc::{channel, Sender, TryRecvError},
};
use std::thread::{sleep, spawn};
use std::time::Duration;

#[derive(Debug, Clone)]
struct Message(String);

#[derive(Debug, Clone, PartialEq)]
struct TaskID(usize);

#[derive(Debug)]
enum Commands {
    /// Shutdown command
    PoisenPill,
    PoisenPillAck(TaskID),
    Register(TaskID, Sender<Commands>),
    /// Serve History or new Messages
    Messages(Vec<Message>),
    NewMessage(Message),
}
use Commands::*;

#[derive(Debug, PartialEq)]
enum WaitReasons {
    Both,
    Network,
    World,
    Nothing,
}
use WaitReasons::*;

impl std::ops::AddAssign for WaitReasons {
    fn add_assign(&mut self, other: WaitReasons) {
        *self = match (&self, other) {
            (Nothing, other) => other,
            (Network, World) => Both,
            (World, Network) => Both,
            (_, _) => return,
        };
    }
}

static TASK_ID_COUNTER: AtomicUsize = ATOMIC_USIZE_INIT;

fn main() {
    let port = "[::]:2000";
    println!("bound port {}", port);

    let listener = TcpListener::bind(port).unwrap();

    let (world, hub) = channel();
    let hub_join_handler = spawn(move || {
        let mut history = Vec::new();
        let mut active_connections = Vec::new();
        for command in hub.iter() {
            match command {
                Register(task_id, chan) => {
                    chan.send(Messages(history.clone()))
                        .expect("new connection already died");
                    active_connections.push((task_id, chan));
                }
                NewMessage(message) => {
                    println!("broadcasting: {:?}", message);
                    history.push(message.clone());
                    // broadcast
                    active_connections
                        .retain(|(_, chan)| chan.send(Messages(vec![message.clone()])).is_ok())
                }
                PoisenPill => {
                    // broadcast
                    active_connections.retain(|(_, chan)| chan.send(PoisenPill).is_ok());
                    if active_connections.len() == 0 {
                        // random sleep so the other thread can shutdown and cleanup
                        sleep(Duration::from_millis(10));
                        return;
                    }
                }
                PoisenPillAck(task_id) => {
                    active_connections.retain(|(id, _)| *id != task_id);

                    if active_connections.len() == 0 {
                        // random sleep so the other thread can shutdown and cleanup
                        sleep(Duration::from_millis(10));
                        return;
                    }
                }
                Messages(_) => panic!("must never receive batched history"),
            }
        }
    });

    {
        use std::os::unix::io::AsRawFd;
        let world_signal = world.clone();
        let listener = listener.as_raw_fd();
        simple_signal::set_handler(
            &[
                simple_signal::Signal::Int, /*simple_signal::Signal::Term*/
            ],
            move |signals| {
                println!("Caught: {:?}", signals);
                world_signal
                    .send(PoisenPill)
                    .expect("unable to shutdown world");

                let result = unsafe { libc::shutdown(listener, libc::SHUT_RDWR) };
                if result != 0 {
                    panic!("unable to shutdown listener")
                }
            },
        );
    }

    while let Ok((stream, _addr)) = listener.accept() {
        let world = world.clone();
        spawn(move || {
            let mut stream = stream;
            handle_client(&mut stream, world);
        });
    }

    println!("waiting for world to shutdown...");
    hub_join_handler.join().expect("hub crashed");
}

fn handle_client(stream: &mut TcpStream, world: Sender<Commands>) {
    let my_task_id = TaskID(TASK_ID_COUNTER.fetch_add(1, Ordering::SeqCst));
    println!("handle_client {:?}", my_task_id);
    let (tx, rx) = channel();

    world
        .send(Register(my_task_id.clone(), tx))
        .expect("unable to register");

    stream
        .set_nonblocking(true)
        .expect("set_nonblocking call failed");
    let mut wait_for = Nothing;

    let mut thread_buffer = Vec::new();
    loop {
        print!(",");
        let mut buf = [0; 32];
        match stream.read(&mut buf) {
            Ok(length) => {
                if length == 0 {
                    // the cannels will cause an error the next time a broadcast occurs and then GC themselfs
                    //world.send(PoisenPillSelf(my_task_id)).expect("unable to shutdown self");
                    return;
                }
                thread_buffer.extend_from_slice(&buf[..length]);
                println!("processing {} with {:?}", length, &buf[..length]);
                let mut last_start = 0;
                for (c, i) in thread_buffer.iter().zip(0..) {
                    if *c == '\n' as u8 {
                        let message = String::from_utf8_lossy(&thread_buffer[last_start..=i]);

                        world
                            .send(NewMessage(Message(message.to_string())))
                            .expect("unable to send NewMessage to world");

                        last_start = i;
                    }
                }

                if last_start + 1 != thread_buffer.len() {
                    // Throw away processed Data
                    println!(
                        "taking over a bit ({}..{})",
                        last_start,
                        thread_buffer.len()
                    );
                    thread_buffer = thread_buffer[last_start..].to_vec();
                } else {
                    thread_buffer.clear();
                }
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                // wait until network socket is ready, typically implemented
                // via platform-specific APIs such as epoll or IOCP
                wait_for += Network;
            }
            Err(e) => panic!("encountered IO error: {}", e),
        };

        match rx.try_recv() {
            Ok(command) => match command {
                PoisenPill => {
                    world
                        .send(PoisenPillAck(my_task_id))
                        .expect("unable to confirm PoisenPill");
                    return;
                }
                Messages(history) => {
                    for msg in history.iter() {
                        stream
                            .write(msg.0.as_bytes())
                            .expect("unable to send history");
                    }
                }
                c => panic!("received invalid command: {:?}", c),
            },
            Err(TryRecvError::Empty) => {
                wait_for += World;
            }
            Err(TryRecvError::Disconnected) => {
                // world has gone away
                return;
            }
        };

        match wait_for {
            Both => {
                sleep(Duration::from_millis(1000));
                print!(":");
            }
            _ => {
                sleep(Duration::from_millis(100));
                print!(".");
            }
        }
        wait_for = Nothing;
        io::stdout().flush().expect("unable to flush stdout");
    }
}

#[cfg(test)]
mod tests {
    use *;

    #[test]
    fn add_assign_a() {
        let mut t = Nothing;

        t += World;
        assert_eq!(World, t);

        t += Network;
        assert_eq!(Both, t);
    }
    #[test]
    fn add_assign_b() {
        let mut t = Nothing;

        t += Network;
        assert_eq!(Network, t);

        t += World;
        assert_eq!(Both, t);
    }

}
