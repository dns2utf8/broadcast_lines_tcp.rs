# broadcast_lines_tcp

Apart from the cleanup code this project is `std` only.

This project runs on Linux only, because of the clean-shutdown code that relies on signals and libc to wakeup the blocked thread waiting in accept.

# Running it

```bash
cargo build
target/debug/broadcast_lines_tcp
```

Then in other consoles, at least 2:
```
nc localhost 2000
```

To shutdown the server, just press `Ctrl + C` in the first shell.
